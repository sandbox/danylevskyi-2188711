Commerce Order Status By Role
======================
Provides ability to determine access to commerce order statuses by role
on the order edit form. It can be useful if you would like to grant only
specific role with ability to set some order statuses.

Installation and configuration
==============================
1. Download Commerce Order Status By Role.
2. Enable the Commerce Order and Commerce Order Status By Role modules.
3. Go to "people > permissions" and set desirable roles in front
   of "Access commerce order {STATUS} status" permission.
