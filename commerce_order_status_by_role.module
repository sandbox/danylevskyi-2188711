<?php

/**
 * @file
 * Provides ability to determine access to commerce order statuses by role on the order edit form.
 */

/**
 * Implements hook_permission().
 */
function commerce_order_status_by_role_permission() {
  $permissions = array();
  $statuses = commerce_order_statuses($conditions = array());
  foreach ($statuses as $status) {
    $permission_name = "access commerce order {$status['name']} status";
    $permissions[$permission_name] = array(
      'title' => t('Access commerce order !status status', array('!status' => $status['name'])),
      'description' => t('Adds ability to access commerce order !status status.', array('!status' => $status['name'])),
    );
  }
  return $permissions;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_order_status_by_role_form_commerce_order_ui_order_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#entity']->type) && $form['#entity']->type == 'commerce_order') {
    foreach ($form['order_status']['status']['#options'] as $state => $statuses) {
      foreach ($statuses as $status_name => $status_title) {
        $permission_name = "access commerce order {$status_name} status";
        if (!user_access($permission_name)) {
          unset($form['order_status']['status']['#options'][$state][$status_name]);
        }
      }
      // Remove empty option groups.
      if (empty($form['order_status']['status']['#options'][$state])) {
        unset($form['order_status']['status']['#options'][$state]);
      }
    }
  }
}
